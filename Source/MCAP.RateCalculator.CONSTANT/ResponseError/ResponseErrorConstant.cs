﻿namespace MCAP.RateCalculator.CONSTANT.ResponseError
{
    public static class ResponseErrorConstant
    {
        public const string USER_IS_NOT_AUTHENTICATED = "The user is not authenticated. Use the ValidateUserMethod before using this one.";
        public const string WRONG_USER_NAME_OR_PASSWORD = "The user name or password is not valid.";
        public const string NULL_HEADER = "The header object was null";
        public const string NULL_OR_EMPTY_USER_NAME = "The user name was null or empty";
        public const string NULL_OR_EMPTY_PASSWORD = "The password was null or empty";
    }
}
