﻿namespace MCAP.RateCalculator.CONSTANT.AppSettings
{
    public static class AppSettingsConstant
    {
        public const string SERVICE_USER_NAME = "Service.UserName";
        public const string SERVICE_PASSWORD = "Service.Password";
    }
}
