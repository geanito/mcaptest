﻿using System;
using System.Globalization;

namespace MCAP.RateCalculator.DOMAIN
{
    public class Rate
    {
        public Rate()
        {

        }

        public Rate(Guid gId, string szProduct, decimal dcProductRate, string szProgram, decimal dcProgramRateAdjustment, string dtPeriodStart, string dtPeriodEnd)
        {
            Id = gId;
            m_Product = szProduct;
            m_ProductRate = dcProductRate;
            m_Program = szProgram;
            m_ProgramRateAdjustment = dcProgramRateAdjustment;
            m_PeriodStart = DateTime.ParseExact(dtPeriodStart, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            m_PeriodEnd = DateTime.ParseExact(dtPeriodEnd, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public Guid Id { get; set; }
        public string m_Product { get; set; }
        public decimal m_ProductRate { get; set; }
        public string m_Program { get; set; }
        public decimal m_ProgramRateAdjustment { get; set; }
        public DateTime m_PeriodStart { get; set; }
        public DateTime m_PeriodEnd { get; set; }

    }
}
