﻿using System;

namespace MCAP.RateCalculator.DOMAIN
{
    public class WebServiceUser
    {
        public Guid Id { get; set; }
        public string m_UserName { get; set; }
        public string m_Password { get; set; }

        public WebServiceUser()
        {
        }

        public WebServiceUser(Guid id, string szUserName, string szPassword)
        {
            Id = id;
            m_UserName = szUserName;
            m_Password = szPassword;
        }

        /// <summary>
        /// Checks if the password provided by the client matches the one in the server
        /// </summary>
        /// <param name="szPassword">User provided password</param>
        /// <returns></returns>
        public bool IsPasswordValid(string szPassword)
        {
            return m_Password == szPassword;
        }
    }
}
