﻿using System;
using System.ComponentModel;

namespace MCAP.RateCalculator.DOMAIN
{
    public class DateRange
    {
        public DateTime m_StartDate { get; set; }

        public DateTime m_EndDate { get; set; }

        public DateTime m_MinEndDate { get; set; }
    }
}
