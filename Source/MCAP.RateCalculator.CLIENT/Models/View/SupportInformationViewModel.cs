﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MCAP.RateCalculator.CLIENT.Models.View
{
    public class SupportInformationViewModel
    {
        [Display(Name = "Product: ")]
        public List<SelectListItem> m_Products { get; set; }
        public string m_SelectedProduct { get; set; }

        [Display(Name = "Program: ")]
        public List<SelectListItem> m_Programs { get; set; }
        public string m_SelectedProgram { get; set; }

        [Display(Name = "Start Period: ")]
        public DateTime m_StartDate { get; set; }

        [Required]
        [DisplayName("Start Period")]
        public string m_SelectedStartDate { get; set; }

        public DateTime m_MinEndDate { get; set; }

        [Display(Name = "End Period: ")]
        public DateTime m_EndDate { get; set; }

        [Required]
        [DisplayName("End Period")]
        public string m_SelectedEndDate { get; set; }

        [Display(Name = "Best Rate")]
        public string m_BestRate { get; set; }

        public bool m_HasBestRate => !string.IsNullOrEmpty(m_BestRate);

        public SupportInformationViewModel()
        {

        }

        public SupportInformationViewModel(List<string> lstProducts, List<string> lstPrograms, DateTime dtStartDate, DateTime dtEndDate, DateTime dtMinEndDate)
        {
            m_Products = GetSelectItemList(lstProducts);
            m_Programs = GetSelectItemList(lstPrograms);
            m_StartDate = dtStartDate;
            m_EndDate = dtEndDate;
            m_MinEndDate = dtMinEndDate;
        }

        private List<SelectListItem> GetSelectItemList(List<string> stringList)
        {
            var selectList = new List<SelectListItem>();

            foreach (var stringEntry in stringList)
            {
                var entry = new SelectListItem()
                {
                    Text = stringEntry,
                    Value = stringEntry
                };

                selectList.Add(entry);
            }

            return selectList;
        }
    }
}