﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace MCAP.RateCalculator.CLIENT.Models.Input
{
    public class RateQuoteInputModel
    {
        [Required]
        public string m_SelectedProduct { get; set; }

        [Required]
        public string m_SelectedProgram { get; set; }

        [Required]
        [DisplayName("Start Date")]
        public string m_SelectedStartDate { get; set; }

        [Required]
        [DisplayName("End Date")]
        public string m_SelectedEndDate { get; set; }

        /// <summary>
        /// Converts the string input of the m_SelectedStartDate into a DateTime
        /// </summary>
        public DateTime m_ConvertedStartDate => !string.IsNullOrEmpty(m_SelectedStartDate) 
            ? DateTime.ParseExact(m_SelectedStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture) 
            : new DateTime();

        public DateTime m_ConvertedEndDate => !string.IsNullOrEmpty(m_SelectedEndDate)
            ? DateTime.ParseExact($"{m_SelectedEndDate} 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture)
            : new DateTime();

    }
}