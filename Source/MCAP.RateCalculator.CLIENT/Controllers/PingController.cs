﻿using System.Web.Mvc;
namespace MCAP.RateCalculator.CLIENT.Controllers
{
    /// <summary>
    /// Used to check if the WebService is responding.
    /// </summary>
    [RoutePrefix("Ping")]
    public class PingController : RateCalculatorBaseController
    {
        [Route("")]
        [HttpGet]
        public string Index()
        {
            return ServiceClient.Ping();
        }
    }
}