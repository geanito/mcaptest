﻿using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MCAP.RateCalculator.CLIENT.Models.Input;
using MCAP.RateCalculator.CLIENT.Models.View;
using MCAP.RateCalculator.CLIENT.RateCalculatorService;
using MCAP.RateCalculator.CONSTANT.AppSettings;

namespace MCAP.RateCalculator.CLIENT.Controllers
{
    public class RateCalculatorBaseController : Controller
    {
        public RateCalculatorSoapClient ServiceClient = new RateCalculatorSoapClient();
        public AuthenticationHeader Authentication = new AuthenticationHeader();

        public RateCalculatorBaseController()
        {
            GetWebServiceCredentials();
            AuthorizeServiceUser();
        }

        /// <summary>
        /// Retrieves the user credetials used to Authenticate the user at the WebService.
        /// </summary>
        private void GetWebServiceCredentials()
        {
            Authentication.m_UserName = ConfigurationManager.AppSettings[AppSettingsConstant.SERVICE_USER_NAME];
            Authentication.m_Password = ConfigurationManager.AppSettings[AppSettingsConstant.SERVICE_PASSWORD];
        }

        /// <summary>
        /// Uses the credetians to authorize the user at the WebService.
        /// </summary>
        /// <returns>A token that must be used to make all the calls to the WebService</returns>
        public AuthenticationHeader AuthorizeServiceUser()
        {
            var requestToken = ServiceClient.ValidateUser(Authentication);

            if (!requestToken.m_HasError)
            {
                Authentication.m_Token = requestToken.m_Token;
            }

            return Authentication;
        }

        /// <summary>
        /// Retrives the support information used to fill the driopdowns and datepickers in the underwritter tool.
        /// </summary>
        /// <returns>A SupportInformationViewModel object that contains a list of available Products and Programs, the possible start date, minimum end date, and maximum end date.</returns>
        public SupportInformationViewModel GetSupportInformation()
        {
            var serviceSupportInformation = ServiceClient.GetSupportInformation(Authentication);

            var supportInformation = new SupportInformationViewModel(
                serviceSupportInformation.m_Products.ToList(),
                serviceSupportInformation.m_Programs.ToList(),
                serviceSupportInformation.DateRange.m_StartDate.AddDays(1),
                serviceSupportInformation.DateRange.m_EndDate,
                serviceSupportInformation.DateRange.m_MinEndDate.AddDays(1));

            return supportInformation;
        }

        /// <summary>
        /// Queries the WebService using the data input of the client to get the best rate given a date
        /// </summary>
        /// <param name="rateQuoteInputModel">Input parameters for retrieving the best rate, needs a Product, a Program, a StartDate, and a End Date.</param>
        /// <returns>Returns a string representing the best rate.</returns>
        public string GetBestRate(RateQuoteInputModel rateQuoteInputModel)
        {
            var szBestRate = string.Empty;

            var rateResult = ServiceClient.GetLowestRate(Authentication,
                rateQuoteInputModel.m_SelectedProduct,
                rateQuoteInputModel.m_SelectedProgram,
                rateQuoteInputModel.m_ConvertedStartDate,
                rateQuoteInputModel.m_ConvertedEndDate);

            if (!rateResult.m_HasError)
            {
                szBestRate = rateResult.Rate;
            }

            return szBestRate;
        }
    }
}