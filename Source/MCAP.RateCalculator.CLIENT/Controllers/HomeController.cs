﻿using System.Web.Mvc;
using MCAP.RateCalculator.CLIENT.Models.Input;

namespace MCAP.RateCalculator.CLIENT.Controllers
{
    [RoutePrefix("Home")]
    public class HomeController : RateCalculatorBaseController
    {
        [Route("Index")]
        [HttpGet]
        public ActionResult Index()
        {
            var supportInformation = GetSupportInformation();

            return View(supportInformation);
        }

        [Route("Search")]
        [HttpPost]
        public ActionResult Search(RateQuoteInputModel rateQuoteInputModel)
        {
            if (!ModelState.IsValid)
            {
                var validationSupportInformation = GetSupportInformation();
                return RedirectToAction("Index", validationSupportInformation);
            }

            var bestRate = GetBestRate(rateQuoteInputModel);

            var supportInformation = GetSupportInformation();
            supportInformation.m_BestRate = bestRate;

            return View("Index", supportInformation);
        }
    }
}