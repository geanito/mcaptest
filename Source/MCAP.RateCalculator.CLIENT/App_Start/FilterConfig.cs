﻿using System.Web;
using System.Web.Mvc;

namespace MCAP.RateCalculator.CLIENT
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
