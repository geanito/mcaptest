﻿using System.Web.Services.Protocols;

namespace MCAP.RateCalculator.SOAP.Header
{
    public class AuthenticationHeader : SoapHeader
    {
        public string m_UserName { get; set; }
        public string m_Password { get; set; }
        public string m_Token { get; set; }
    }
}