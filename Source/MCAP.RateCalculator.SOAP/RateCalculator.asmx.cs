﻿using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using MCAP.RateCalculator.CONSTANT.ResponseError;
using MCAP.RateCalculator.SOAP.Response;

namespace MCAP.RateCalculator.SOAP
{
    [WebService(Namespace = "http://mcap.test.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class RateCalculator : WebServiceBase
    {
        /// <summary>
        /// Gets the support information used by a client to fill a form with valid information to be used in queries to the WebService
        /// </summary>
        /// <returns>A SupportInformationResult object containing data used to fill a form with valid information to be used in queries to the WebService</returns>
        [WebMethod(Description = "Retrieve support information used to fill the client`s page and to to query back valid rates from the WebService")]
        [SoapHeader("Authentication")]
        public SupportInformationResult GetSupportInformation()
        {
            var result = new SupportInformationResult();

            try
            {
                if (IsUserAuthenticated())
                {
                    var lstProducts = UnitOfWork.Rates.GetAvailableProducts();
                    var lstPrograms = UnitOfWork.Rates.GetAvailablePrograms();
                    var dateRange = UnitOfWork.Rates.GetAvailableDateRange();

                    result.m_Products = lstProducts;
                    result.m_Programs = lstPrograms;
                    result.DateRange = dateRange;
                }
                else
                {
                    AddErrorMessage(result, ResponseErrorConstant.USER_IS_NOT_AUTHENTICATED);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex, result);
            }

            return result;
        }


        /// <summary>
        /// Retrieve the lowest rate given a combination of PRODUCT / PROGRAM / START DATE / END DATE.
        /// </summary>
        /// <param name="szProduct">User provided Product</param>
        /// <param name="szProgram">User provided Program</param>
        /// <param name="dtStartDate">User provided Start Date</param>
        /// <param name="dtEndDate">User provided End Date</param>
        /// <returns></returns>
        [WebMethod(Description = "Retrieve the lowest rate given a combination of PRODUCT / PROGRAM / START DATE / END DATE.")]
        [SoapHeader("Authentication")]
        public RateResult GetLowestRate(string szProduct, string szProgram, DateTime dtStartDate, DateTime dtEndDate)
        {
            var result = new RateResult();

            try
            {
                if (IsUserAuthenticated())
                {
                    result.Rate = UnitOfWork.Rates.GetBestRate(szProduct, szProgram, dtStartDate, dtEndDate);
                }
                else
                {
                    AddErrorMessage(result, ResponseErrorConstant.USER_IS_NOT_AUTHENTICATED);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex, result);
            }

            return result;
        }
    }
}
