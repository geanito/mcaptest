﻿using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.SOAP.Response
{
    public class RateResult : BaseResult
    {
        public string Rate { get; set; }
    }
}