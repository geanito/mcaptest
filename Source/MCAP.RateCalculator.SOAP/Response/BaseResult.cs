﻿using System.Collections.Generic;
using System.Linq;

namespace MCAP.RateCalculator.SOAP.Response
{
    public class BaseResult
    {
        public List<string> m_ErrorMesssages { get; set; }
        public bool m_HasError { get; set; }
    }
}