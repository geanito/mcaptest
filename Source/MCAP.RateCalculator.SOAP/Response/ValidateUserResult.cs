﻿namespace MCAP.RateCalculator.SOAP.Response
{
    public class ValidateUserResult : BaseResult
    {
        public string m_Token { get; set; }
    }
}