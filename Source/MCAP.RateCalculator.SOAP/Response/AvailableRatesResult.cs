﻿using System.Collections.Generic;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.SOAP.Response
{
    public class AvailableRatesResult : BaseResult
    {
        public List<Rate> Rates { get; set; }
    }
}