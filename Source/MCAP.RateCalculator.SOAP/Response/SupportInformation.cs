﻿using System.Collections.Generic;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.SOAP.Response
{
    public class SupportInformationResult : BaseResult
    {
        public List<string> m_Products { get; set; }
        public List<string> m_Programs { get; set; }
        public DateRange DateRange { get; set; }


    }
}