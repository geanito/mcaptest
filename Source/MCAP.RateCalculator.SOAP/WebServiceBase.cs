﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using MCAP.RateCalculator.CONSTANT.ResponseError;
using MCAP.RateCalculator.DATA;
using MCAP.RateCalculator.SOAP.Header;
using MCAP.RateCalculator.SOAP.Response;

namespace MCAP.RateCalculator.SOAP
{
    public class WebServiceBase : WebService
    {
        public AuthenticationHeader Authentication;
        public readonly UnitOfWork UnitOfWork = new UnitOfWork(new DataContext());

        /// <summary>
        /// Checks if the provided user credentials are valid
        /// </summary>
        /// <returns>A confirmation that the user is validated by the server or not.</returns>
        public bool IsUserValidated()
        {
            if (Authentication == null) return false;

            var user = UnitOfWork.WebServiceUsers.GetUserByName(Authentication.m_UserName);

            return user != null && user.IsPasswordValid(Authentication.m_Password);
        }

        /// <summary>
        /// Checks if the user provided token exists in the server cache.
        /// </summary>
        /// <returns>A confirmation that the user is validated in the server with a valid token.</returns>
        public bool IsUserAuthenticated()
        {
            return HttpRuntime.Cache[Authentication.m_Token] != null;
        }

        /// <summary>
        /// Generates the Token in the server cache to be used for the client to make the requests.
        /// </summary>
        /// <returns>A server generated token</returns>
        public string GenerateToken()
        {
            var token = Guid.NewGuid().ToString();

            HttpRuntime.Cache.Add(
                token,
                Authentication.m_UserName,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(30),
                System.Web.Caching.CacheItemPriority.NotRemovable,
                null);

            return token;
        }

        /// <summary>
        /// Adds an exception message to a WebService request
        /// </summary>
        /// <param name="ex">The exception thrown by the application</param>
        /// <param name="result">The result object of the request to be filled with the error message.</param>
        public void AddExceptionMessage(Exception ex, BaseResult result)
        {
            result.m_HasError = true;

            if (result.m_ErrorMesssages != null && result.m_ErrorMesssages.Any())
            {
                result.m_ErrorMesssages.Add(ex.Message);
            }
            else
            {
                result.m_ErrorMesssages = new List<string>()
                {
                    ex.Message
                };
            }
        }

        /// <summary>
        /// Adds an exception message to a WebService request
        /// </summary>
        /// <param name="result">The result object of the request to be filled with the error message.</param>
        /// <param name="szErrorMessage">The error message created by the server</param>
        public void AddErrorMessage(BaseResult result, string szErrorMessage)
        {
            result.m_HasError = true;

            if (result.m_ErrorMesssages != null && result.m_ErrorMesssages.Any())
            {
                result.m_ErrorMesssages.Add(szErrorMessage);
            }
            else
            {
                result.m_ErrorMesssages = new List<string>()
                {
                    szErrorMessage
                };
            }
        }

        /// <summary>
        /// Validate user by checking user name and password and retrieve a token in case of success.
        /// </summary>
        /// <returns>Returns a ValidateUserResult object containg the Server generated Token that has to be used to query the WebService</returns>
        [WebMethod(Description = "Validate user by checking user name and password and retrieve a token in case of success.")]
        [SoapHeader("Authentication")]
        public ValidateUserResult ValidateUser()
        {
            var result = new ValidateUserResult();

            try
            {
                if (IsUserValidated())
                {
                    result.m_Token = GenerateToken();
                }
                else
                {
                    AddErrorMessage(result, ResponseErrorConstant.WRONG_USER_NAME_OR_PASSWORD);
                }
            }
            catch (Exception ex)
            {
                AddExceptionMessage(ex, result);
            }

            return result;
        }

        /// <summary>
        /// Checks if the WebService is responsive
        /// </summary>
        /// <returns>The datetime of the WebService with an confirmation message.</returns>
        [WebMethod(Description = "Checks if the WebService is responsive.")]
        public string Ping()
        {
            return $"{DateTime.Now} - MCAP Rate Calculator Web Service is Running.";
        }
    }
}