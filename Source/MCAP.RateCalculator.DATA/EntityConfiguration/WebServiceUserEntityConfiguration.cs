﻿using System.Data.Entity.ModelConfiguration;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.EntityConfiguration
{
    public class WebServiceUserEntityConfiguration : EntityTypeConfiguration<WebServiceUser>
    {
        /// <summary>
        /// WebServiceUser Entity Framework Configuration for Code First
        /// </summary>
        public WebServiceUserEntityConfiguration()
        {
            ToTable("WebServiceUsers");
            HasKey(x => x.Id);
        }
    }
}
