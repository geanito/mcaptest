﻿using System.Data.Entity.ModelConfiguration;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.EntityConfiguration
{
    public class RateEntityConfiguration : EntityTypeConfiguration<Rate>
    {
        /// <summary>
        /// Rate Entity Framework Configuration for Code First
        /// </summary>
        public RateEntityConfiguration()
        {
            ToTable("Rates");
            HasKey(x => x.Id);
        }
    }
}
