﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MCAP.RateCalculator.DATA.Core.RepositoryInterface
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
