﻿using System;
using System.Collections.Generic;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Core.RepositoryInterface
{
    public interface IRateRepository : IRepository<Rate>
    {
        /// <summary>
        /// Retrieves a list of all available Products
        /// </summary>
        /// <returns>List of available Products</returns>
        List<string> GetAvailableProducts();

        /// <summary>
        /// Retrieves a list of all available Programs
        /// </summary>
        /// <returns>List of available Programs</returns>
        List<string> GetAvailablePrograms();

        /// <summary>
        /// Gets the best possible rate given a combination of Product, Program, Start Date and End Date
        /// </summary>
        /// <param name="szProduct">Selected Product</param>
        /// <param name="szProgram">Selected Program</param>
        /// <param name="dtStartDate">Selected Start Date</param>
        /// <param name="dtEndDate">Selected End Date</param>
        /// <returns>The best rate</returns>
        string GetBestRate(string szProduct, string szProgram, DateTime dtStartDate, DateTime dtEndDate);

        /// <summary>
        /// Gets the date range where the client can search and find a valid rate
        /// </summary>
        /// <returns>Returns a valid possible date range that the client can use to search a rate.</returns>
        DateRange GetAvailableDateRange();
    }
}