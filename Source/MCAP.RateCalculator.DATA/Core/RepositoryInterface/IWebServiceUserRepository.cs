﻿using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Core.RepositoryInterface
{
    public interface IWebServiceUserRepository : IRepository<WebServiceUser>
    {
        /// <summary>
        /// Gets the registered username that can query the WebService
        /// </summary>
        /// <param name="szUserName">The registered UserName</param>
        /// <returns>Returns a WebServiceUser object that contains the information of the registered user.</returns>
        WebServiceUser GetUserByName(string szUserName);
    }
}
