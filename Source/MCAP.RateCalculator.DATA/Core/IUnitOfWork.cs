﻿using System;
using MCAP.RateCalculator.DATA.Core.RepositoryInterface;

namespace MCAP.RateCalculator.DATA.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IRateRepository Rates { get; }
        IWebServiceUserRepository WebServiceUsers { get; }
        int Save();
    }
}
