﻿using System.Data.Entity;
using MCAP.RateCalculator.CONSTANT.ConnectionString;
using MCAP.RateCalculator.DATA.EntityConfiguration;
using MCAP.RateCalculator.DATA.Initializer;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA
{
    public class DataContext : DbContext
    {
        public DataContext() : base(ConnectionStringConstant.RATE_CALCULATOR_CONNECTION_STRING)
        {
            Database.SetInitializer(new RateDbInitializer());
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Rate> Rates { get; set; }
        public virtual DbSet<WebServiceUser> WebServiceUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new RateEntityConfiguration());
            modelBuilder.Configurations.Add(new WebServiceUserEntityConfiguration());
        }
    }
}
