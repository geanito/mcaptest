﻿using MCAP.RateCalculator.DATA.Core;
using MCAP.RateCalculator.DATA.Core.RepositoryInterface;
using MCAP.RateCalculator.DATA.Persistence.Repository;

namespace MCAP.RateCalculator.DATA
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;
        public IRateRepository Rates { get; }
        public IWebServiceUserRepository WebServiceUsers { get; }

        public UnitOfWork(DataContext context)
        {
            _context = context;
            Rates = new RateRepository(_context);
            WebServiceUsers = new WebServiceUserRepository(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
