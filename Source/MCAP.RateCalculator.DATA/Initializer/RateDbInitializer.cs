﻿using System.Data.Entity;
using System.Linq;

namespace MCAP.RateCalculator.DATA.Initializer
{
    public class RateDbInitializer : DropCreateDatabaseIfModelChanges<DataContext>
    {
        /// <summary>
        /// Inserts mock data into the database
        /// </summary>
        /// <param name="context">The EntityDBContext</param>
        protected override void Seed(DataContext context)
        {
            CreateRatesDataSample(context);
            CreateWebServiceUserDataSample(context);

            base.Seed(context);
        }

        /// <summary>
        /// Adds a set of Rates based on the document sent to be used as test data.s
        /// </summary>
        /// <param name="context">The EntityDBContext</param>
        private void CreateRatesDataSample(DataContext context)
        {
            context.Rates.AddRange(RateData.Rates.ToList());
        }

        /// <summary>
        /// Add a WebServiceUser to be used as test data..
        /// </summary>
        /// <param name="context">The EntityDBContext</param>
        private void CreateWebServiceUserDataSample(DataContext context)
        {
            context.WebServiceUsers.Add(WebServiceUserData.WebServiceUser);
        }
    }
}
