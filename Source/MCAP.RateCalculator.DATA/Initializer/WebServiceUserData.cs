﻿using System;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Initializer
{
    public class WebServiceUserData
    {
        /// <summary>
        /// Data entry used to test the WebService
        /// </summary>
        public static WebServiceUser WebServiceUser = new WebServiceUser(new Guid("cd3cdcea-dff7-4ae3-ac09-649ef6760987"), "mcap", "Mcap123" ); 
    }
}
