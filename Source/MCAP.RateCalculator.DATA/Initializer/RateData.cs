﻿using System;
using System.Collections.Generic;
using MCAP.RateCalculator.CONSTANT.Rate;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Initializer
{
    public static class RateData
    {
        /// <summary>
        /// Data entry used to fill the database with the rate during initial database creation.
        /// </summary>
        public static IList<Rate> Rates = new List<Rate>()
        {
            new Rate(new Guid("85754c4a-290d-48af-85f1-b0a3772bd700"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.7, ProgramConstant.STANDARD, (decimal)-0.05, "01/09/2018 00:00:00", "14/09/2018 23:59:59"), 
            new Rate(new Guid("356422a9-24bc-4755-a4d5-1dcc3bc13c69"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.7, ProgramConstant.QUICK_CLOSE, (decimal)-0.1, "01/09/2018 00:00:00", "14/09/2018 23:59:59"), 
            new Rate(new Guid("07565d06-eb04-420b-8b86-819dc5c0324a"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.04, ProgramConstant.STANDARD, (decimal)-0.05, "01/09/2018 00:00:00", "14/09/2018 23:59:59"), 
            new Rate(new Guid("5196da2d-f07b-4498-aa6b-143fa73c7f46"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.04, ProgramConstant.QUICK_CLOSE, (decimal)-0.1, "01/09/2018 00:00:00", "14/09/2018 23:59:59"),
            new Rate(new Guid("a60a4506-e7fd-4a03-9172-9ec6b1960f9a"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.8, ProgramConstant.STANDARD, (decimal)-0.15, "15/09/2018 00:00:00", "30/09/2018 23:59:59"),
            new Rate(new Guid("09b726c8-8ed1-4edc-9057-1f30cbc97850"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.8, ProgramConstant.QUICK_CLOSE, (decimal)-0.22, "15/09/2018 00:00:00", "30/09/2018 23:59:59"),
            new Rate(new Guid("5812fee0-5e20-4e9a-95e0-a78ab7f93195"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.04, ProgramConstant.STANDARD, (decimal)-0.15, "15/09/2018 00:00:00", "30/09/2018 23:59:59"),
            new Rate(new Guid("ef07db18-c8db-41ba-884f-fe93649a7a0a"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.04, ProgramConstant.QUICK_CLOSE, (decimal)-0.22, "15/09/2018 00:00:00", "30/09/2018 23:59:59"),
            new Rate(new Guid("c7d454e0-2594-476d-9cd7-8af45959cda3"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.8, ProgramConstant.STANDARD, (decimal)-0.15, "01/10/2018 00:00:00", "02/10/2018 23:59:59"),
            new Rate(new Guid("d90df80d-c94f-4db9-b313-635cd702b710"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.8, ProgramConstant.QUICK_CLOSE, (decimal)-0.2, "01/10/2018 00:00:00", "02/10/2018 23:59:59"),
            new Rate(new Guid("90dee3ba-cc58-4bd1-9631-8d74c99d9c37"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.STANDARD, (decimal)-0.15, "01/10/2018 00:00:00", "02/10/2018 23:59:59"),
            new Rate(new Guid("8a690f55-6d64-4406-8d42-c9ffda460eed"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.QUICK_CLOSE, (decimal)-0.2, "01/10/2018 00:00:00", "02/10/2018 23:59:59"),
            new Rate(new Guid("146d6cf3-2cdc-456a-8d59-3b7dab3837fd"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.75, ProgramConstant.STANDARD, (decimal)-0.15, "03/10/2018 00:00:00", "31/10/2018 23:59:59"),
            new Rate(new Guid("1630c128-dbd0-45f4-8665-a87d714ce4a8"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.75, ProgramConstant.QUICK_CLOSE, (decimal)-0.15, "03/10/2018 00:00:00", "31/10/2018 23:59:59"),
            new Rate(new Guid("1465f145-04e0-4b7c-a66d-c0e8b17938e5"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.STANDARD, (decimal)-0.15, "03/10/2018 00:00:00", "31/10/2018 23:59:59"),
            new Rate(new Guid("81dec570-a040-4880-97cf-a864a07b10f3"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.QUICK_CLOSE, (decimal)-0.15, "03/10/2018 00:00:00", "31/10/2018 23:59:59"),
            new Rate(new Guid("51acc90b-d7ee-4c17-8f47-9a9c3fd11897"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.75, ProgramConstant.STANDARD, (decimal)-0.05, "01/11/2018 00:00:00", "31/12/2018 23:59:59"),
            new Rate(new Guid("905016e4-0758-4047-ad1c-80dd85e5bbc7"), ProductConstant.THREE_YEAR_CLOSED, (decimal)2.75, ProgramConstant.QUICK_CLOSE, (decimal)-0.1, "01/11/2018 00:00:00", "31/12/2018 23:59:59"),
            new Rate(new Guid("30db6cd8-2253-4000-a76c-aa0d2fc4c264"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.STANDARD, (decimal)-0.05, "01/11/2018 00:00:00", "31/12/2018 23:59:59"),
            new Rate(new Guid("0de168fe-2c6e-4f32-a3a3-46d73d9b179e"), ProductConstant.FIVE_YEAR_CLOSED, (decimal)3.02, ProgramConstant.QUICK_CLOSE, (decimal)-0.1, "01/11/2018 00:00:00", "31/12/2018 23:59:59"),
        };
    }
}
