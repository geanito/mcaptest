﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using MCAP.RateCalculator.DATA.Core.RepositoryInterface;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Persistence.Repository
{
    public class RateRepository : Repository<Rate>, IRateRepository
    {
        public DataContext DataContext => Context as DataContext;

        public RateRepository(DataContext context) : base(context)
        {
        }

        /// <summary>
        /// Retrieves a list of all available Products
        /// </summary>
        /// <returns>List of available Products</returns>
        public List<string> GetAvailableProducts()
        {
            return DataContext.Rates.Select(x => x.m_Product).Distinct().ToList();
        }

        /// <summary>
        /// Retrieves a list of all available Programs
        /// </summary>
        /// <returns>List of available Programs</returns>
        public List<string> GetAvailablePrograms()
        {
            return DataContext.Rates.Select(x => x.m_Program).Distinct().ToList();
        }

        /// <summary>
        /// Gets the best possible rate given a combination of Product, Program, Start Date and End Date
        /// </summary>
        /// <param name="szProduct">Selected Product</param>
        /// <param name="szProgram">Selected Program</param>
        /// <param name="dtStartDate">Selected Start Date</param>
        /// <param name="dtEndDate">Selected End Date</param>
        /// <returns>The best rate</returns>
        public string GetBestRate(string szProduct, string szProgram, DateTime dtStartTime, DateTime dtEndTime)
        {
            var rateCalculationResults = new Dictionary<Guid, decimal>();

            var dtScopeRates =  DataContext.Rates
                .Where(x => x.m_Product == szProduct && x.m_Program == szProgram && x.m_PeriodStart >= dtStartTime && x.m_PeriodEnd <= dtEndTime)
                .OrderBy(x => x.m_Product)
                .ThenBy(x => x.m_Program)
                .ToList();

            foreach (var dtScopeRate in dtScopeRates)
            {
                var finalRate = dtScopeRate.m_ProductRate - Math.Abs(dtScopeRate.m_ProgramRateAdjustment);

                rateCalculationResults.Add(dtScopeRate.Id, finalRate);
            }

            var sortedRates = rateCalculationResults.OrderBy(x => x.Value);

            var lowestRate = sortedRates.FirstOrDefault();

            return lowestRate.Value.ToString(CultureInfo.InvariantCulture);
        }


        /// <summary>
        /// Gets the date range where the client can search and find a valid rate
        /// </summary>
        /// <returns>Returns a valid possible date range that the client can use to search a rate.</returns>
        public DateRange GetAvailableDateRange()
        {
            var dsRates = DataContext.Rates.ToList();

            var dtLowestDate = dsRates.OrderBy(x=> x.m_PeriodStart).Select(x => x.m_PeriodStart).FirstOrDefault();
            var dtHighestDate = dsRates.OrderByDescending(x=>x.m_PeriodEnd).Select(x => x.m_PeriodEnd).FirstOrDefault();
            var dtMinEndDate = dsRates.OrderBy(x=>x.m_PeriodEnd).Select(x => x.m_PeriodEnd).FirstOrDefault();

            return new DateRange()
            {
                m_StartDate = dtLowestDate,
                m_EndDate = dtHighestDate,
                m_MinEndDate = dtMinEndDate
            };
        }

    }
}
