﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MCAP.RateCalculator.DATA.Core.RepositoryInterface;

namespace MCAP.RateCalculator.DATA.Persistence.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Finds a Entity based on the predicate filter parameters
        /// </summary>
        /// <param name="predicate">The filter parameter to execute the query</param>
        /// <returns></returns>
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate);
        }

        /// <summary>
        /// inds a Entity based on the predicate filter parameters
        /// </summary>
        /// <param name="predicate">The filter parameter to execute the query</param>
        /// <returns></returns>
        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().SingleOrDefault(predicate);
        }
    }
}
