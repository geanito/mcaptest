﻿using System.Data.Entity;
using System.Linq;
using MCAP.RateCalculator.DATA.Core.RepositoryInterface;
using MCAP.RateCalculator.DOMAIN;

namespace MCAP.RateCalculator.DATA.Persistence.Repository
{
    public class WebServiceUserRepository : Repository<WebServiceUser>, IWebServiceUserRepository
    {
        public DataContext DataContext => Context as DataContext;

        public WebServiceUserRepository(DbContext context) : base(context)
        {
        }

        /// <summary>
        /// Gets the registered username that can query the WebService
        /// </summary>
        /// <param name="szUserName">The registered UserName</param>
        /// <returns>Returns a WebServiceUser object that contains the information of the registered user.</returns>
        public WebServiceUser GetUserByName(string szUserName)
        {
            return DataContext.WebServiceUsers.FirstOrDefault(x => x.m_UserName == szUserName);
        }
    }
}
